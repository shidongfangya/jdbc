package jdbcutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;


public class JDBCUtil {
	
	private static ResourceBundle rb = ResourceBundle.getBundle("test1");
	private static String driver = rb.getString("driver");
	private static String url = rb.getString("url");
	private static String user = rb.getString("user");
	private static String pass = rb.getString("password");
	
	private JDBCUtil() {
		//保证该类不能在外部实例化对象
	}
	
	static {
		// 驱动 -- 只需要执行一次就可以
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static Connection getConnection() {
		try {
			return DriverManager.getConnection(url, user, pass);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 关闭
	public static void close(Connection conn, Statement st) { // Statement 更好，PreparedStatement可以向上转型
		close(conn, st, null);// 增删改
	}

	public static void close(Connection conn, Statement st, ResultSet rs) {
		try {// 查
			if (st != null) {
				st.close();
				st = null;
			}
			if (conn != null) {
				conn.close();
				conn = null;
			}
			if (rs != null) {
				rs.close();
				rs = null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws SQLException {
		Connection conn = JDBCUtil.getConnection();
		System.out.println(conn);
		
		PreparedStatement ps = conn.prepareStatement("");
		JDBCUtil.close(conn, null);
	}
}
