package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import jdbcutil.JDBCUtil;


public class JDBCDaoB {//Statement
	Connection conn = null;
	Statement st = null;
	
	public void insert() throws SQLException {
		
		
		conn = JDBCUtil.getConnection();
		st = conn.createStatement();
		
		conn.setAutoCommit(false);	// 取消自动提交 --开启事务
		for(int i=1;i<=10;i++ ) {
			String sql = "insert into test_demo1.employee(code,name,gender) values('code"+i+"','李华"+i+"号','gend"+2*i+"')";
			st.addBatch(sql); // 批量处理提供的操作
		}
		int[] result = st.executeBatch();
		
		st.clearBatch();
		conn.commit();// 提交事务
		JDBCUtil.close(conn, st);
		
		System.out.println(result.length);
	}
	
	public void delete() throws SQLException {
		
		
		conn = JDBCUtil.getConnection();
		st = conn.createStatement();
		
		conn.setAutoCommit(false);
		for(int i=3;i<=5;i++) {
			String sql = "delete from test_demo1.employee where code = 'code"+i+"'";
			st.addBatch(sql); // 批量处理提供的操作
		}
		int[] result = st.executeBatch();
		
		st.clearBatch();
		conn.commit();// 提交事务
		JDBCUtil.close(conn, st);
		
		System.out.println(result.length);
	}
	
	public void update() throws SQLException {
		
		
		conn = JDBCUtil.getConnection();
		st = conn.createStatement();
		
		conn.setAutoCommit(false);
		for(int i=2,j=1;i<=5;i++,j++) {
			String sql = "update test_demo1.employee set name='张伟"+j+"号',gender ='gend"+3*i+"' where code='code"+i+"'";
			st.addBatch(sql); // 批量处理提供的操作
		}
		int[] result = st.executeBatch();
		
		st.clearBatch();
		conn.commit();// 提交事务
		JDBCUtil.close(conn, st);
		
		System.out.println(result.length);
	}
}
